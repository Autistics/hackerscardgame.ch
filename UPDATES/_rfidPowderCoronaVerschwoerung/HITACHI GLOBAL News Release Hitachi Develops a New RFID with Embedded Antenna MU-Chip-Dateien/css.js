//
var agent = navigator.userAgent;
var appve = navigator.appVersion;
var appna = navigator.appName;
var userOs = null;
var browser = null;
var version = null;
var cssSrc = null;
//JUDGE_OS
if( agent.indexOf('Win') != -1 ){
	userOs = "Win";
}
else if( agent.indexOf('Mac') != -1 ){
	userOs = "Mac";
}
else{
	userOs = "other";
}
//JUDGE_BROWSER
if( appna == "Microsoft Internet Explorer" ){
	browser = "MSIE";
	version = agent.substring((agent.indexOf('MSIE') + 5),(agent.indexOf('MSIE') + 8));
}
else if( appna == "Netscape" ){
	browser = "NN";
	version = appve.substring( 0,3 );
}
// STYLESHEET
var cssSrc ;
if ( userOs == "Mac" ){
	cssSrc = '<link rel="stylesheet" type="text/css" href="/images/css/default_mac.css">';
}
else if ( userOs == "Win" && browser == "NN" ){
	cssSrc = '<link rel="stylesheet" type="text/css" href="/images/css/default_win_nn.css">';
}
else if ( userOs == "Win" && browser == "MSIE" ){
	cssSrc = '<link rel="stylesheet" type="text/css" href="/images/css/default_win_ie.css">';
}
else{
	cssSrc = '<link rel="stylesheet" type="text/css" href="/images/css/default_win_other.css">';
}
document.write( cssSrc ) ;
function common_pop_open(dest_url,width_value,height_value) {
	var ppt_window = window.open(dest_url,'ppt_window','width='+width_value+',height='+height_value);
	ppt_window.window.focus();
}